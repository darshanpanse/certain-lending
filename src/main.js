import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
import Properties from './components/Properties.vue'
import Content from './components/Content.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);
Vue.use(Vuetify);


new Vue({
  el: '#app',
  render: h => h(App)
})

new Vue({
  el: '#home',
  render: h => h(Content)
})

new Vue({
  el:'#propertyDetails',
  render:h => h(Properties)
})
